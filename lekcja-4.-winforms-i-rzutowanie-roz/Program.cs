﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lekcja_4._winforms_i_rzutowanie_roz
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Związku z precyzją wyniku, do wszelkich operacji na danych wrażliwych, takich jak pięniądze
             * należy używać typu decimal, który oferuje najwiekszą dokładność z wszystkich typów
             * zmienno przecinkówych dostępnych w C#
             */
            const decimal kursEuro = 4.24m;

            Console.WriteLine("Ile złotówek chcesz wymienić na euro?");
            decimal pln = decimal.Parse(Console.ReadLine());
            decimal euro = pln / kursEuro;

            /* To polecenie wpisze nam pełną ilość euro */
            Console.WriteLine("Otrzymasz {0} euro", euro);

            /*
             * Za pomocą ":F" można modyfikować dokłaność wypisywanej liczby, czyli zaokrąglenie ilość liczb po przecinku
             * W domyśle ":F" zaokrągluje liczbę do dwóch miejsc po przecinku
             */
            Console.WriteLine("Otrzymasz {0:F} euro", euro);

            /*
             * Możemy sterować ilością zaokrąglenia miejsc po przecinku umieszając dodatkowe liczby po ":F"
             * na przykład dla czter miejsc po przecinku należy podać ":F4"
             */
            Console.WriteLine("Otrzymasz {0:F4} euro", euro);

            Console.ReadKey();

        }
    }
}
